﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Data.SqlClient.SqlConnection
Public Class Patient_Register

    '//*> Initialization of Private Member's of Class
    Private Property Gender As String
    Private Property rbtn_male As String
    Private Property rbtn_female As String

    Private Sub Patient_Register_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btn_register_Click(sender As System.Object, e As System.EventArgs) Handles btn_register.Click
        ' Dim strConnString As String = ConfigurationManager.ConnectionStrings("Data Source=Ajay;Initial Catalog=Seva_Dham;Integrated Security=True").ConnectionString

        'Dim con As New SqlConnection(strConnString)
        Dim con As New SqlConnection("Data Source=LAPTOP-UPH019VB;Initial Catalog=Seva_dham_backup;Integrated Security=True")
        Dim cmd As New SqlCommand()

        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "sp_insert_regis"
        cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = txtname.Text.Trim()
        cmd.Parameters.Add("@DOB", SqlDbType.DateTime).Value = DateTimePicker_dob.Text.Trim()
        cmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = RichTextBox_address.Text.Trim()
        cmd.Parameters.Add("@Valid_From", SqlDbType.Date).Value = DateTimePicker_valid_from.Text.Trim()
        cmd.Parameters.Add("@Valid_Till", SqlDbType.Date).Value = DateTimePicker_valid_till.Text.Trim()

        ' Dim rbtm As String = ""
        'If rbtn_male.Checked = True Then
        'rbtn_male = ("Male")
        'Else : rbtn_female.Checked = True
        'rbtn_female = "Female"
        'End If

        cmd.Parameters.Add("@Gender", SqlDbType.NChar).Value = radio_btn_male.Text.Trim()
        cmd.Parameters.Add("@Visit_Counter", SqlDbType.Int).Value = ddl_v_counter.Text.Trim()
        cmd.Parameters.Add("@Mobile_No", SqlDbType.Int).Value = TextBox_mob_no.Text.Trim()
        cmd.Parameters.Add("@Blood_Group", SqlDbType.NVarChar).Value = ComboBox_blood_grp.Text.Trim()
        cmd.Connection = con

        Try
            con.Open()

            If cmd.ExecuteNonQuery() = 1 Then
                MessageBox.Show(" Record inserted successfully !! ")
            Else
                MessageBox.Show(" Registration Failed !! ")
            End If

        Catch ex As Exception

            Throw ex

        Finally

            con.Close()
            con.Dispose()

        End Try
    End Sub
End Class